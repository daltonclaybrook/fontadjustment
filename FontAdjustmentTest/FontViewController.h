//
//  FontViewController.h
//  FontAdjustmentTest
//
//  Created by Dalton Claybrook on 6/10/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomLabel.h"

@interface FontViewController : UIViewController

@property (nonatomic, weak) IBOutlet CustomLabel *label1;
@property (nonatomic, weak) IBOutlet CustomLabel *label2;
@property (nonatomic, weak) IBOutlet CustomLabel *label3;
@property (nonatomic, weak) IBOutlet CustomLabel *label4;

@property (nonatomic, weak) IBOutlet UIView *baselineView;

- (void)configureWithFont:(UIFont *)font;
- (void)configureWithTextAlignment:(NSTextAlignment)alignment;
- (void)configureWithLabelYShiftPercent:(CGFloat)yShift;
- (void)configureWithLineSpacing:(CGFloat)spacing;

@end
