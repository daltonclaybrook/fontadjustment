//
//  UIView+BRAAutolayoutExtensions.h
//  Watcher
//
//  Created by Dalton Claybrook on 12/18/13.
//  Copyright (c) 2013 Bottle Rocket Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BRAAutolayoutExtensions)

- (void)constrainEdgesToSuperview;
- (void)constrainEdgesToSuperviewWithInsets:(UIEdgeInsets)insets;

@end
