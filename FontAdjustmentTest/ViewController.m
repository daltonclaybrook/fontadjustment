//
//  ViewController.m
//  FontAdjustmentTest
//
//  Created by Dalton Claybrook on 6/10/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "ViewController.h"
#import "FontViewController.h"

static NSString * const kEmbedFontVC1SegueID = @"kEmbedFontVC1SegueID";
static NSString * const kEmbedFontVC2SegueID = @"kEmbedFontVC2SegueID";

@interface ViewController ()

@property (nonatomic, strong) FontViewController *fontVC1;
@property (nonatomic, strong) FontViewController *fontVC2;

@end

@implementation ViewController

#pragma mark - Superclass

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureFonts];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    if ([segue.identifier isEqualToString:kEmbedFontVC1SegueID])
    {
        self.fontVC1 = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:kEmbedFontVC2SegueID])
    {
        self.fontVC2 = segue.destinationViewController;
    }
}

#pragma mark - Private

- (void)configureFonts
{
    UIFontDescriptor *gothamDescriptor = [UIFontDescriptor fontDescriptorWithName:@"Gotham Bold" size:17];
    UIFont *gothamFont = [UIFont fontWithDescriptor:gothamDescriptor size:40.0];
    
    UIFontDescriptor *codeProDescriptor = [UIFontDescriptor fontDescriptorWithName:@"Code Pro Bold LC" size:17.0];
    UIFont *codeProFont = [UIFont fontWithDescriptor:codeProDescriptor size:40.0];
    
    self.fontVC1.label1.text = @"JjGgYy";
    self.fontVC2.label1.text = @"JjGgYy";
    
    [self.fontVC1 configureWithFont:gothamFont];
    [self.fontVC2 configureWithFont:codeProFont];
    
    [self.fontVC1 configureWithTextAlignment:NSTextAlignmentRight];
    [self.fontVC2 configureWithLabelYShiftPercent:0.13];
    
    NSString *string = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
    self.fontVC1.label2.text = string;
    self.fontVC2.label2.text = string;
    
    [self.fontVC2 configureWithLineSpacing:20.0];
}

@end
