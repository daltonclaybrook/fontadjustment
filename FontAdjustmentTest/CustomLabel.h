//
//  CustomLabel.h
//  FontAdjustmentTest
//
//  Created by Dalton Claybrook on 6/10/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLabel : UILabel

@property (nonatomic, assign) CGFloat yShiftPercent;

@end
