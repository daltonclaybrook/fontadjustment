//
//  CustomLabel.m
//  FontAdjustmentTest
//
//  Created by Dalton Claybrook on 6/10/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "CustomLabel.h"
#import "UIView+BRAAutolayoutExtensions.h"

@interface CustomLabel ()

@property (nonatomic, assign) dispatch_once_t onceToken;
@property (nonatomic, assign) CGAffineTransform identityTransform;

@property (nonatomic, strong) UIView *baselineView;
@property (nonatomic, strong) NSLayoutConstraint *bottomConstraint;

@end

@implementation CustomLabel

#pragma mark - Initializers

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self CustomLabelCommonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self CustomLabelCommonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self CustomLabelCommonInit];
    }
    return self;
}

- (void)CustomLabelCommonInit
{
    _baselineView = [[UIView alloc] init];
    _baselineView.backgroundColor = [UIColor clearColor];
    _baselineView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_baselineView];
    [self setupConstraintsForBaselineView:_baselineView];
}

#pragma mark - Accessors

- (void)setYShiftPercent:(CGFloat)yShift
{
    _yShiftPercent = yShift;
    
    self.bottomConstraint.constant = [self computedShift];
    [self invalidateIntrinsicContentSize];
    [self setNeedsDisplay];
}

#pragma mark - Superclass

- (CGSize)intrinsicContentSize
{
    CGSize size = [super intrinsicContentSize];
    CGFloat shift = [self computedShift];
    if (shift > 0)
    {
        size.height += ([self computedShift] * 2 + 1.0); // extra 1.0 seems to be necessary.
    }
    return size;
}

- (void)drawTextInRect:(CGRect)rect
{
    rect.origin.y += [self computedShift];
    [super drawTextInRect:rect];
}

#pragma mark - Private

- (CGFloat)computedShift
{
    return self.font.pointSize * self.yShiftPercent;
}

- (void)setupConstraintsForBaselineView:(UIView *)view
{
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:view.superview attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:view.superview attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeRight multiplier:1.0 constant:0];
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:view.superview attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
    self.bottomConstraint = [NSLayoutConstraint constraintWithItem:view.superview attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    
    [self addConstraints:@[ topConstraint, rightConstraint, leftConstraint, self.bottomConstraint ]];
}

@end
