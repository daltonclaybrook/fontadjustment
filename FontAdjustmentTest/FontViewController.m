//
//  FontViewController.m
//  FontAdjustmentTest
//
//  Created by Dalton Claybrook on 6/10/15.
//  Copyright (c) 2015 Bottle Rocket, LLC. All rights reserved.
//

#import "FontViewController.h"

@implementation FontViewController

#pragma mark - Superclass

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self.baselineView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.label1 attribute:NSLayoutAttributeBaseline multiplier:1.0 constant:0.0];
    [self.view addConstraint:constraint];
}

#pragma mark - Public

- (void)configureWithFont:(UIFont *)font
{
    self.label1.font = font;
    self.label2.font = font;
    self.label3.font = font;
    self.label4.font = font;
}

- (void)configureWithTextAlignment:(NSTextAlignment)alignment
{
    self.label1.textAlignment = alignment;
    self.label2.textAlignment = alignment;
    self.label3.textAlignment = alignment;
    self.label4.textAlignment = alignment;
}

- (void)configureWithLabelYShiftPercent:(CGFloat)yShift
{
    self.label1.yShiftPercent = yShift;
    self.label2.yShiftPercent = yShift;
    self.label3.yShiftPercent = yShift;
    self.label4.yShiftPercent = yShift;
}

- (void)configureWithLineSpacing:(CGFloat)spacing
{
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = spacing;
    NSDictionary *attributes = @{ NSParagraphStyleAttributeName : style };
    
    self.label2.attributedText = [[NSAttributedString alloc] initWithString:self.label2.text attributes:attributes];
}

@end
